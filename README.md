﻿#Final Project

In this project, we will create a git repository with a commit history similar to the graph and table below:

```mermaid
graph RL
H((H))-->A((A))
v1[v1.00]-->H
v2[v1.01]-->L((L))
L-->H
K((K))-->H
L-->K
E((E))-->B
B((B))-->A
G((G))-->E
H-->G
I((I))-->E
M((M))-->I
C((C))-->B
D((D))-->C
E-->D
I-->G
F((F))-.->E
F1((F1))-.->I
F2((F2))-.->M
style A fill:#94c37f,stroke:#7f,stroke-width:4px
style H fill:#94c37f,stroke:#7f,stroke-width:4px
style L fill:#94c37f,stroke:#7f,stroke-width:4px
style K fill:#f8111a,stroke:#7f,stroke-width:4px
style G fill:#a2c4c9,stroke:#7f,stroke-width:4px
style B fill:#b6a9d4,stroke:#7f,stroke-width:4px
style E fill:#b6a9d4,stroke:#7f,stroke-width:4px
style I fill:#b6a9d4,stroke:#7f,stroke-width:4px
style M fill:#b6a9d4,stroke:#7f,stroke-width:4px
style C fill:#fea72c,stroke:#7f,stroke-width:4px
style D fill:#fea72c,stroke:#7f,stroke-width:4px
style F fill:#fea72c,stroke:#7f,stroke-width:4px,stroke-dasharray: 10, 10
style F1 fill:#fea72c,stroke:#7f,stroke-width:4px,stroke-dasharray: 10, 10
style F2 fill:#fea72c,stroke:#7f,stroke-width:4px,stroke-dasharray: 10, 10
style v1 fill:#fef064,stroke:#7f,stroke-width:4px
style v2 fill:#fef064,stroke:#7f,stroke-width:4px



| commit |     type     |           fileA.txt content           | create  on  branch |            commit message            | parents |                notes                |
|:------:|:------------:|:-------------------------------------:|:------------------:|:------------------------------------:|:-------:|:-----------------------------------:|
|    A   |              |                   -                   |       master       |             add README.md            |    -    |           create README.md          |
|    B   |              |             ""empty string            |       develop      |             add fileA.txt            |    A    |                                     |
|    C   |              |            "feature 1 wip"            |      feature 1     |             feature 1 wip            |    B    |                                     |
|    D   |              |        "feature 1 with 2 bugs"        |      feature 1     |             add feature 1            |    C    |                                     |
|    E   | merge commit |              (no change)              |       develop      | Merge branch 'feature1' into develop |   B,D   |        delete feature1 label        |
|    F   |              | "feature 1 with 2 bugs feature 2 wip" |      feature 2     |             feature 2 wip            |    E    |                                     |
|    G   |              |         "feature 1 with 1 bug"        |      release 1     |          fix feature 1 bug X         |    E    |                                     |
|    H   | merge commit |   results in "feature 1 with 1 bug"   |       master       |        Merge branch 'release1'       |   A,G   |             tag "v1.00"             |
|    I   | merge commit |   results in "feature 1 with 1 bug"   |       develop      | Merge branch 'release1' into develop |   E,G   |        delete release1 label        |
|   F1   |    rebase    |  "feature 1 with 1 bug feature 2 wip" |      feature 2     |             feature 2 wip            |    I    | will need to resolve merge conflict |
|    K   |              |              "feature 1"              |      hotfix 1      |          fix feature 1 bug Y         |    H    |                                     |
|    L   | merge commit |              "feature 1"              |       master       |        Merge branch 'hotfix1'        |   H,K   |             tag "v1.01"             |
|    M   | merge commit |              "feature 1"              |       develop      |  Merge branch 'hotfix1' into develop |   I,K   |         delete hotfix1 label        |
|   F2   |    rebase    |       "feature 1 feature 2 wip"       |      feature 2     |             feature 2 wip            |    M    | will need to resolve merge conflict |